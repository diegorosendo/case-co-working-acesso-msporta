package br.com.itau.porta.models.dto;

import br.com.itau.porta.models.Porta;
import org.springframework.stereotype.Component;

@Component
public class PortaMapper {
    public Porta toPorta(CreatedPortaRequest createdPortaRequest){
        Porta porta = new Porta();
        porta.setAndar(createdPortaRequest.getAndar());
        porta.setSala(createdPortaRequest.getSala());
        return porta;
    }
}