package br.com.itau.porta.models.dto;

public class CreatedPortaRequest {

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    private String andar;

    private String sala;

}
