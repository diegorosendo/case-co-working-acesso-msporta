package br.com.itau.porta.repository;

import br.com.itau.porta.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Long> {
}
