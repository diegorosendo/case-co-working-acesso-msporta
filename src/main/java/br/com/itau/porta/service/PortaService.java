package br.com.itau.porta.service;

import br.com.itau.porta.exceptions.PortaNotFoundException;
import br.com.itau.porta.models.Porta;
import br.com.itau.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    PortaRepository portaRepository;

    public Porta create(Porta porta) {
        return portaRepository.save(porta);
    }

    public Porta getById(Long id) {
        Optional<Porta> byId = portaRepository.findById(id);

        if(!byId.isPresent()) {
            throw new PortaNotFoundException();
        }

        return byId.get();
    }

}
