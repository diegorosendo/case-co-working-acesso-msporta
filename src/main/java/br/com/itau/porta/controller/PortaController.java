package br.com.itau.porta.controller;

import br.com.itau.porta.models.Porta;
import br.com.itau.porta.models.dto.CreatedPortaRequest;
import br.com.itau.porta.models.dto.PortaMapper;
import br.com.itau.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    PortaService portaService;

    @Autowired
    private PortaMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta create(@RequestBody CreatedPortaRequest createdPortaRequest){
        Porta porta = mapper.toPorta(createdPortaRequest);

        porta = portaService.create(porta);

        return porta;
    }

    @GetMapping("{id}")
    public Porta getById(@PathVariable Long id){
        return portaService.getById(id);
    }

}
